<?php

namespace app\models;
use \yii\base\Object;
use yii\db\ActiveRecord;

class Student extends ActiveRecord
{
	const STATUS_INACTIVE = 0;
	const STATUS_ACTIVE = 1;
	
	public static function tableName()
	{
		return 'students';
	}
	
	public function getName($id){
		$student = new Student();
		$student = Student::find()
		->where(['id' => $id])
		->one();
		return $student->name;
	}

}
